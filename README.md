# Terraform-GCP-GitLab-Pipeline

Ce projet de démarrage vise à créer un pipeline CI/CD en utilisant Terraform et GitLab pour provisionner et gérer des ressources sur Google Cloud Platform (GCP). Les ressources créées comprennent des buckets de stockage, des fonctions Cloud et des triggers Pub/Sub et Cloud Scheduler.

## Objectif

L'objectif de ce projet est de créer un pipeline automatisé pour déployer et gérer des ressources sur Google Cloud Platform en utilisant Terraform et GitLab. Le pipeline permettra de valider, planifier et appliquer les changements de configuration aux ressources GCP.
## Prérequis

- Un compte Google Cloud Platform (GCP) avec un projet configuré.
- Un compte GitLab avec un dépôt pour héberger votre code Terraform.
- Un fichier de clés de compte de service pour votre projet GCP (au format JSON).

## Structure du projet

### Description des fichiers

- `.gitlab-ci.yml` : Contient la configuration du pipeline GitLab CI/CD. Le pipeline est divisé en trois étapes : validate, plan et apply. Chaque étape exécute différentes commandes Terraform pour valider, planifier et appliquer les changements aux ressources GCP.
- `backend.tf` : Configure le backend Terraform. Dans cet exemple, un backend local est utilisé, mais vous pouvez configurer un backend distant, comme GCS, pour stocker l'état de Terraform.
- `function.tf` : Crée les fonctions Cloud et les archives des fichiers source. Ce fichier contient des blocs de ressources pour créer des fonctions Cloud, des objets de bucket de stockage et des archives de fichiers source pour chaque fonction Cloud.
- `main.tf` : Configure le provider GCP et les informations d'identification. Ce fichier contient la configuration du provider GCP, qui utilise un fichier JSON de clés de compte de service pour s'authentifier auprès de GCP.
- `pubsub.tf` : Crée les topics Pub/Sub et les jobs Cloud Scheduler. Ce fichier contient des blocs de ressources pour créer des topics Pub/Sub et des jobs Cloud Scheduler, qui déclenchent les fonctions Cloud.
- `storage.tf` : Crée les buckets de stockage. Ce fichier contient des blocs de ressources pour créer des buckets de stockage GCS, qui sont utilisés pour stocker les fichiers source des fonctions Cloud.
- `variables.tf` : Définit les variables utilisées dans les fichiers Terraform. Ce fichier contient des variables pour le projet GCP, la région, les informations d'identification de la base de données et les variables d'environnement des fonctions Cloud.
- `src_*` : Contient les fichiers source des fonctions Cloud. Chaque dossier `src_*` contient un fichier `main.py`, qui est le point d'entrée de la fonction Cloud.

## Utilisation

1. Clonez ce dépôt dans votre propre compte GitLab.
2. Ajoutez vos clés de compte de service GCP au dépôt en tant que variables d'environnement. Vous pouvez le faire en accédant à l'onglet "Settings" > "CI/CD" > "Variables" de votre dépôt GitLab.
3. Modifiez les fichiers Terraform selon vos besoins. Assurez-vous de fournir des valeurs pour les variables définies dans `variables.tf`.
4. Poussez vos modifications dans votre dépôt GitLab.
5. Le pipeline GitLab se déclenchera automatiquement et créera les ressources sur GCP.

## Bonnes pratiques

Pour maintenir un projet Terraform propre et bien structuré, il est important de suivre certaines bonnes pratiques. Voici quelques recommandations pour travailler avec Terraform, GitLab et GCP :

1. Utilisez des branches pour organiser votre travail. Créez des branches distinctes pour les environnements de développement, de validation et de production. Chaque environnement doit avoir sa propre configuration Terraform et ses propres variables d'environnement.
2. Travaillez en équipe et collaborez sur les modifications. Revoyez et discutez des modifications apportées au code Terraform avant de les fusionner dans les branches principales.
3. Utilisez des tags pour versionner vos configurations Terraform. Lorsque vous déployez une nouvelle version de votre infrastructure, créez un tag Git pour identifier la version spécifique de la configuration Terraform.
4. Documentez vos configurations Terraform. Assurez-vous de fournir des commentaires et une documentation claire pour expliquer l'utilisation et la raison d'être de chaque ressource et module Terraform.
5. Utilisez des modules Terraform pour réutiliser et partager des configurations. Les modules permettent de simplifier et de réutiliser des configurations Terraform pour des ressources similaires, ce qui facilite la maintenance et la mise à jour de votre infrastructure.

## Prochaines étapes

- Mettre en œuvre les bonnes pratiques mentionnées ci-dessus pour organiser et structurer votre projet Terraform.
- Ajoutez des tests automatisés pour valider et vérifier les configurations Terraform avant de les déployer.
- Intégrez d'autres outils de gestion de l'infrastructure, tels que l'analyse de l'état de Terraform, pour surveiller et gérer votre infrastructure GCP.

## Bonnes pratiques de programmation

Scalabilité dans le développement logiciel signifie généralement que le logiciel doit fonctionner comme prévu, même lorsqu'il fonctionne à une échelle supérieure.

Voici quelques aspects importants à prendre en compte pour assurer la scalabilité :

1. Scalabilité du code - le code peut devenir énorme : Le code est écrit une fois, mais il est modifié de nombreuses fois. Les exigences changent souvent avec le temps, donc le code doit répondre à ces exigences et pouvoir être modifié facilement, clairement et efficacement.
   - Facilement : On ne devrait pas avoir à réécrire/refactorer le vieux code pour ajouter ou modifier une fonctionnalité. Cela augmente la portée des tests, car il faudrait également tester la fonctionnalité du vieux code. Astuce : apprenez et suivez les principes et modèles de conception logicielle en général.
   - Efficacement : On devrait être capable d'apporter des changements sans trop d'effort ou sans écrire de code en double. Cela signifie que si un code a déjà été écrit quelque part, "on devrait être capable de le réutiliser au lieu de le copier". Quelques astuces simples :
     - Créez des API pour les opérations courantes et utilisez-les partout où cela est nécessaire.
     - Utilisez l'héritage et suivez les principes et modèles de conception logicielle.

2. Scalabilité de l'utilisation - l'utilisation peut augmenter : Le logiciel doit être capable de supporter une utilisation à grande échelle. Par exemple, un grand nombre d'utilisateurs/connexions simultanées à un serveur Web. On doit être capable d'augmenter la capacité du logiciel avec un peu d'ajout de ressources (voir la mise à l'échelle verticale et horizontale).

3. Scalabilité dans le temps - les données peuvent devenir énormes : Avec le temps, les données peuvent devenir énormes, ce qui peut entraîner des opérations CRUD plus lentes (en fonction du système de base de données). La maintenance (sauvegarde, migration, modification) de ces données énormes devient également difficile. Dans ce cas, la conception de la base de données et la sélection d'un système de base de données particulier deviennent cruciales.

En conclusion, l'utilisation des concepts de conception logicielle, de conception de bases de données et de sélection des technologies peut réellement améliorer la qualité, la scalabilité et la performance d'un système. En tant que concepteurs/développeurs de logiciels, nous devrions tous apprendre ces techniques pour construire de meilleurs systèmes.

Merci d'avoir lu.:)

