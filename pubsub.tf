# Define a Google Cloud Pub/Sub topic as a resource

resource "google_pubsub_topic" "topic"  {
   name= "${var.project_id}-topic"
}


# Define a Google Cloud Scheduler job as a resource
# The "job" resource is defined with the name "${var.project_id}-job", a description, and a schedule to run every 5 minutes
# The job uses a Pub/Sub target to send a message to the topic defined above
resource "google_cloud_scheduler_job" "job"  {
   
   name="${var.project_id}-job"
   description="test job"
   schedule="*/5 * * * *"
   
   pubsub_target {

    topic_name= "${google_pubsub_topic.topic.id}"
    data    = base64encode("hello-from-terraform")

    }

    }
