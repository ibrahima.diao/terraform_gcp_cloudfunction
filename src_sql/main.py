from google.cloud import secretmanager

def get_secret_data(project_id,secret_id,version_id):
 

  client=secretmanager.SecretManagerServiceClient()

  secret_detail=f"projects/{project_id}/secrets/{secret_id}/versions/{version_id}"

  response=client.access_secret_version(request={"name":secret_detail})

  data=response.payload.data.decode("UTF-8")

  print("Data: {}".format(data))

  return data

def secret(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    project_id="vote-for-innov"
    secret_id='db-pass'
    version_id=1
    return get_secret_data(project_id,secret_id,version_id)